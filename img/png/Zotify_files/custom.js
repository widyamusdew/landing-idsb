$(document).ready(function(){


  $('#first').click(function(){
    $('#img-first').addClass('getaccess')
    $('#arrow-first').addClass('purplearrow')
     $('#img-first').removeClass('timecaro-white')
     $('#img-second, #img-third, #img-four, #img-five').addClass('timecaro-white')
    $('#img-second, #img-third, #img-four, #img-five').removeClass('getaccess')
    $('#arrow-second, #arrow-third, #arrow-four, #arrow-five').removeClass('purplearrow')

  })
  $('#second').click(function(){
    $('#img-second').addClass('getaccess')
    $('#arrow-second').addClass('purplearrow')
     $('#img-second').removeClass('timecaro-white')
     $('#img-first, #img-third, #img-four, #img-five').addClass('timecaro-white')
    $('#img-first, #img-third, #img-four, #img-five').removeClass('getaccess')
    $('#arrow-first, #arrow-third, #arrow-four, #arrow-five').removeClass('purplearrow')

  })
  $('#third').click(function(){
    $('#img-third').addClass('getaccess')
    $('#arrow-third').addClass('purplearrow')
    $('#img-third').removeClass('timecaro-white')
    $('#img-second, #img-first, #img-four, #img-five').addClass('timecaro-white')
    $('#img-second, #img-first, #img-four, #img-five').removeClass('getaccess')
    $('#arrow-second, #arrow-first, #arrow-four, #arrow-five').removeClass('purplearrow')

  })
  $('#four').click(function(){
    $('#img-four').addClass('getaccess')
    $('#arrow-four').addClass('purplearrow')
    $('#img-four').removeClass('timecaro-white')
    $('#img-second, #img-first, #img-third, #img-five').addClass('timecaro-white')
    $('#img-second, #img-first, #img-third, #img-five').removeClass('getaccess')
    $('#arrow-second, #arrow-first, #arrow-third, #arrow-five').removeClass('purplearrow')

  })
  $('#five').click(function(){
    $('#img-five').addClass('getaccess')
    $('#arrow-five').addClass('purplearrow')
    $('#img-five').removeClass('timecaro-white')
    $('#img-second, #img-first, #img-third, #img-four').addClass('timecaro-white')
    $('#img-second, #img-first, #img-third, #img-four').removeClass('getaccess')
    $('#arrow-second, #arrow-first, #arrow-third, #arrow-four').removeClass('purplearrow')

  })
})
$(document).ready(function () {
$(document).on("scroll", onScroll);

    //smoothscroll
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
var scrollPos = $(document).scrollTop();
$('#menu-center a').each(function () {
    var currLink = $(this);
    var refElement = $(currLink.attr("href"));
    if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
        $('#menu-center ul li a').removeClass("active");
        currLink.addClass("active");
    }
    else{
        currLink.removeClass("active");
    }
});
}
